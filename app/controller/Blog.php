<?php

class Blog extends Controller{

    public function index(){
        $data["blog"] = $this->model("Blog_model")->getBlogAndUser();
        

        $this->view("blog/index", $data);
        
    }

    public function model($model) 
    {
        require_once '../app/model/' . $model . '.php';
        return new $model;
    }
}