<?php

class App{

    protected $controller = "Home";
    protected $method = "Index";
    protected $params = [];

    function __construct()
    {
        $url = $this->parseUrl();

        // var_dump($url);
        
        if(isset($url)){
            if(file_exists("../app/controller/" . $url[0] . ".php")){
                // var_dump($url);

                $this->controller = $url[0]; 
                unset($url[0]); // remove data arr pada index ke 0

                // echo "exists";
            }
        }

        // panggil class php, sesuai url path yang datang dari request GET
        require_once "../app/controller/" . $this->controller . ".php";
        $this->controller = new $this->controller(); // sama dengan" $this->controller = new Home(); -> tergantung isi string controller

        // memanggil method
        if(isset($url[1])){
            if(method_exists($this->controller, $url[1])){
                $this->method = $url[1];
                echo " ini method: " . $this->method;
                unset($url[1]);
            }
        }

        if(!empty($url)){
            var_dump($url);
            $this->params = array_values($url);
        }

        call_user_func_array([$this->controller, $this->method], $this->params);

    }

    function parseUrl(){
        if(isset($_GET["url"])){
            $url = rtrim($_GET["url"], "/");
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode("/", $url);

            return $url;
        }
    }

}