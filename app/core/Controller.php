<?php

class Controller{

    function view($view, $data = []){
        require_once "../app/view/" . $view . ".php";
    }

    function model($model){
        require_once "../app/model/" . $model . ".php";
        return new $model;
    }

}